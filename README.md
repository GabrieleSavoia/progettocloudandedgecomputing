# Progetto Cloud and Edge Computing

## Gabriele Savoia

### (Progetto base: https://gitlab.com/cicciodev/cloudedgecomputing/-/tree/02-django-base-project?ref_type=heads)

In questo progetto e' stato utilizzato Docker per creare
i container necessari all’esecuzione (sia in development che in production). Sono stati poi definiti
opportuni script in grado di eseguire l’applicazione in maniera automatizzata e di settare le variabili d’ambiente opportune. Grazie poi alle funzionalità di Gitlab è stata creata una pipeline CI/CD
così da poter automatizzare gli stage di test, build e deploy: per quest’ultimo è stato deciso di eseguire un’istanza EC2 di AWS così da poter simulare in maniera realistica lo stage di production
dell’applicazione.


### Esecuzione del progetto in development
```
$ ./start.sh dev
```

### URL sito in produzione

http://3.250.238.204
